# SateCommon
This gem is common implementation to display success and failure response messages in a form of json string. This includes method response to return both and util class to extract errors from model classes.

## Usage
In order to use this gem first we have to put this statement at the top of our controller class require 'sate/common/methodresponse' and/or require 'sate/common/util', then define the MethodResponse class with new keyword by passing the appropriate values for success, message, data, errors, and total. And for Util class we have to access error_messages function by passing the object name to get the encountered error on that specific model.  

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'sate_common'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install sate_common
```

## Contributing
Contribution directions go here.

## License
sate_common is copyright &copy; 2018 SATE Sol. It is free software, and may be redistributed  under the terms specified in the [MIT License](http://opensource.org/licenses/MIT) file.
