require 'spec_helper'

describe SateCommon do
  it 'has a version number' do
    expect(Sate::Common::VERSION).not_to be nil
  end

  it 'allows creating a new method response' do
    response = Sate::Common::MethodResponse.new(true, "test message", nil, 0)
    expect(response.success).to eq true
  end

  it 'converts objects to hash' do
    obj = Sate::Common::MethodResponse.new(true, "test message", "test data", 0)
    expect(obj.to_hash.has_key? "success").to be_truthy
    expect(obj.to_hash.has_key? "message").to be_truthy
    expect(obj.to_hash.has_key? "data").to be_truthy
    expect(obj.to_hash.has_key? "total").to be_truthy
  end

  it "create object rom json string" do
    data = {"success" => true, "message" => "test message", "data" => "test data",
            "total" => 0,"errors" => ["Error 1", "Error 2"]}
    json = JSON.generate(data)
    response = Sate::Common::MethodResponse.from_json json
    expect(response.success).to be_truthy
    expect(response.message).to eq "test message"
    expect(response.data).to eq "test data"
    expect(response.total).to eq 0
    expect(response.errors.count).to eq 2
  end

end
