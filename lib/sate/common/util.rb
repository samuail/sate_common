module Sate
  module Common
    class Util
      def self.error_messages obj, name=obj.class.name
        obj.errors.full_messages.inject([]) { |list, msg| list << "#{name} #{msg}"}
      end
    end
  end
end
