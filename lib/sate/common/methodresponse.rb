require 'json'
module Sate
  module Common
    class MethodResponse
      attr_accessor :success, :message, :data, :errors, :total

      def initialize(success = nil, message = nil, data = nil, errors = [], total = nil)
        @success = success
        @message = message
        @data = data
        @errors = errors
        @total = total
      end

      def self.from_json(json_response)
        response = JSON.parse json_response
        success =  response['success'] if response.has_key?('success')
        message =  response['message'] if response.has_key?('message')
        data = response['data'] if response.has_key?('data')
        errors = response['errors'] if response.has_key?('errors')
        total = response['total'] if response.has_key?('total')
        MethodResponse.new(success, message, data, errors, total)
      end

      def to_hash
        hash = {}
        self.instance_variables.each do |var|
          hash[var.to_s.delete("@")] = self.instance_variable_get var
        end
        return hash
      end

    end
  end
end
