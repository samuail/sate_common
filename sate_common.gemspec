$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "sate/common/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "sate_common"
  s.version     = Sate::Common::VERSION
  s.authors     = ["Samuel Teshome"]
  s.email       = ["samuailteshome@yahoo.com"]
  s.homepage    = "http://www.sate.com.et"
  s.summary     = "Common reusable classes for sate"
  s.description = "This is a gem containing common reusable classes for sate."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.0"

  s.add_development_dependency "json", "~> 2.1"
  s.add_development_dependency "rspec-rails", "~> 3.7", ">= 3.7.2"
end
